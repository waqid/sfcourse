<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    /**
     * @Route("/", name="main")
     */
    public function index()
    {
        return new Response(
            '<h1>W'
        );
    }

    /**
     * @Route("/custom/{name?}", name="custom")
     */
    public function custom(Request $request) 
    {
        dump($request);
        return new Response(
            'Ini jj'
        );
    }
}
